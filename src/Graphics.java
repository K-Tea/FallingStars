import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.FallingStars;
import model.Model;
import javafx.scene.image.Image;

public class Graphics {

    public Model getModel() {
        return model;
    }

    public GraphicsContext getGc() {
        return gc;
    }

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;

    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    // Methoden


    public void draw() {


            // Clear Screen and make it black

        drawBackground();


        // Draw Stars

        drawStars();


        // Draw Player
        drawPlayer();

        drawHome();

    }

    public void drawHome() {
        Image home = new Image ("Images/worldwide.png");
        // gc.setFill(Color.AQUAMARINE);
        gc.drawImage (home,
                model.getHomePlanet().getX() - model.getHomePlanet().getW() / 2,
                model.getHomePlanet().getY() - model.getHomePlanet().getH() / 2,
                model.getHomePlanet().getW(),
                model.getHomePlanet().getH()
        );
    }

    public void drawPlayer() {
        //gc.setFill(Color.GREEN);
        Image spaceship = new Image("Images/spaceship1.png");
        gc.drawImage(spaceship,
                model.getPlayer().getX() - model.getPlayer().getW()/2,
                model.getPlayer().getY() - model.getPlayer().getH()/2,
                model.getPlayer().getW(),
                model.getPlayer().getH()
        );
    }

    public void drawStars() {
        for (FallingStars fallingStars : this.model.getFallingStars()) {
            Image stern = new Image("Images/stern.png");
        //  gc.setFill(Color.YELLOW);
            gc.drawImage(stern,

                    fallingStars.getX() - fallingStars.getW() / 2,
                    fallingStars.getY() - fallingStars.getH() / 2,
                    fallingStars.getW(),
                    fallingStars.getH()
            );
        }
    }

    public void drawBackground() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0,Model.WIDTH,Model.HEIGHT);
    }


}
