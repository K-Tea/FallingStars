package model;


import java.awt.*;

public class FallingStars {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private float speedY;


    // Konstruktoren
    public FallingStars(int x, int y, float speedX) {
        this.x = x;
        this.y = y;
        this.h = 20;
        this.w = 20;
        this.speedY = speedX;
    }

    // Methoden
    public void update(long elapsedTime){
        this.y = Math.round(this.y + elapsedTime* speedY);
    }

    // Setter + Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public float getSpeedY() {
        return speedY;
    }

    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getW(),getH());
    } 
}
    
        
