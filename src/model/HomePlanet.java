package model;

import java.awt.*;

public class HomePlanet {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;


    // Konstruktoren
    public HomePlanet() {
        this.x = 400;
        this.y = 40;
        this.h = 40;
        this.w = 40;

    }

    // Getter + Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public Rectangle getBoundsHome() {
        return new Rectangle(getX(), getY(), getW(),getH());
    }
}
