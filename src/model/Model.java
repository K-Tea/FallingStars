package model;

import com.sun.tools.javac.Main;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

public class Model extends Component {

    // FINALS
    public static final int WIDTH = 500;
    public static final int HEIGHT = 700;

    // Eigenschaften
    private List<FallingStars> fallingStars = new LinkedList<>();
    private Player player;
    private Object Rectangle;
    private HomePlanet homePlanet;

    // Konstruktoren

    public Model() {

        this.fallingStars.add(new FallingStars(100, 20, 0.2f));
        this.fallingStars.add(new FallingStars(50, 30, 0.2f));
        this.fallingStars.add(new FallingStars(100, 40, 0.1f));
        this.fallingStars.add(new FallingStars(200, 50, 0.1f));
        this.fallingStars.add(new FallingStars(410, 100, 0.2f));
        this.fallingStars.add(new FallingStars(450, 40, 0.3f));
        this.fallingStars.add(new FallingStars(30, 10, 0.1f));
        this.fallingStars.add(new FallingStars(300, 10, 0.2f));
        this.fallingStars.add(new FallingStars(15, 0, 0.3f));
        this.fallingStars.add(new FallingStars(10, 10, 0.2f));
        this.fallingStars.add(new FallingStars(10, 10, 0.1f));
        this.fallingStars.add(new FallingStars(480, 10, 0.1f));



            this.player = new Player(250, 660);
            this.homePlanet = new HomePlanet();


    }


    // Methoden
    public void update (long elapsedTime) {

        if (fallingStars.get(0).getY() > HEIGHT)

        { fallingStars.removeAll(fallingStars);

            this.fallingStars.removeAll(this.fallingStars);
            this.fallingStars.add(new FallingStars(310, 20, 0.15F));
            this.fallingStars.add(new FallingStars(0, 30, 0.2F));
            this.fallingStars.add(new FallingStars(120, 40, (float)Math.random()));
            this.fallingStars.add(new FallingStars(230, 50, (float)Math.random()));
            this.fallingStars.add(new FallingStars(30, 0, (float)Math.random()));
            this.fallingStars.add(new FallingStars(480, 40, (float)Math.random()));
            this.fallingStars.add(new FallingStars(30, 10, (float)Math.random()));
            this.fallingStars.add(new FallingStars(320, 10, 0.1F));
            this.fallingStars.add(new FallingStars(370, 10, (float)Math.random()));
            this.fallingStars.add(new FallingStars(410, 10, 0.2F));
            this.fallingStars.add(new FallingStars(60, 0, (float)Math.random()));
            this.fallingStars.add(new FallingStars(450, 40, (float)Math.random()));



        }

    for (FallingStars fallingStars : this.fallingStars) {
        fallingStars.update(elapsedTime);
    }


        collision();



}



    // Setter + Getter
    public List<FallingStars> getFallingStars() {
        return fallingStars;
    }

    public Player getPlayer() {
        return player;
    }

    public HomePlanet getHomePlanet() {
        return homePlanet;
    }

    public void collision() {
        Rectangle r3 = player.getBoundsPlayer();

        for (FallingStars fallingStars : this.fallingStars) {

            Rectangle r2 = fallingStars.getBounds();
            Rectangle r4 = homePlanet.getBoundsHome();

            if (r3.intersects(r2)) {


                gameOver();
            }

            if (r3.intersects(r4)) {

                gameWon();
            }

        }
    }

        public void gameOver() {

//            JOptionPane.showMessageDialog(this, "GAME OVER :-( \nDo you want to try again?", "Game Over", JOptionPane.YES_NO_OPTION);
//            System.exit(ABORT);

            int response = JOptionPane.showConfirmDialog(null, "GAME OVER!\nMöchtest Du weiterspielen?", "Confirm",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.NO_OPTION) {
                System.exit(ABORT);
            }
            else if (response == JOptionPane.YES_OPTION) {

            }

        }

    public void gameWon() {
        JOptionPane.showMessageDialog(this, "Finally home! You WON!!!", "Home Sweet Home", JOptionPane.PLAIN_MESSAGE);
        System.exit(ABORT);

    }

        }



