package model;

import java.awt.*;


public class Player {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    private int health = 3;


    // Konstruktoren
    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 30;
        this.w = 30;

    }

    // Methoden
    public void move (int dx, int dy) {
        if (x> 505) {
            this.x = 1;
        }
        else if (x< 0) {
            this.x = 499;}
        else
            {this.x += dx;}

        if (y<=0) {
            this.y = 2;
        }

        else if (y >= 700) {
            this.y = 698;
        }

        else
             {this.y += dy;}


    }
    public void gameOver() {

    }


    // Getter + Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    Rectangle getBoundsPlayer() {
        return new Rectangle(getX(), getY(), getW(),getH());
    }

}
